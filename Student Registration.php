<?php 
	include('form_validation.php'); 
?>
<html>
	<head>
		<title>Student Registration</title>
		
	</head>
	<body style="background:#DFFEFF">


<div class="sideimg"></div>
<div class="heading">STUDENT<br>REGISTRATION</div>
        
    <form method="post" class="form" >
<table>
<?php
if(isset($error))
{
 ?>
    <tr>
    <td class="error"><?php echo $error; ?></td>
    </tr>
    <?php
}
?>
<tr>
<td><input type="text" name="uid" placeholder="User-ID" value="<?php if(isset($username)){echo $username;} ?>"  <?php if(isset($code) && $code == 1){ echo "autofocus"; }  ?> /></td>
</tr>
<tr>
<td><input type="text" name="email" placeholder="Email"  value="<?php if(isset($email)){echo $email;} ?>" <?php if(isset($code) && $code == 2){ echo "autofocus"; }  ?> /></td>
</tr>

<tr>
<td><input type="" name="ph_no" placeholder="Phone Number" <?php if(isset($code) && $code == 6){ echo "autofocus"; }  ?> /></td>
</tr>

<tr>
<td><input type="" name="name" placeholder="Full Name" <?php if(isset($code) && $code == 7){ echo "autofocus"; }  ?> /></td>
</tr>

<tr>
<td><input type="password" name="password" placeholder="Password" <?php if(isset($code) && $code == 4){ echo "autofocus"; }  ?> /></td>
</tr>
<tr>
<td><input type="password" placeholder="Confirm Password" name="password_2" <?php if(isset($code) && $code == 5){ echo "autofocus"; }  ?> /></td>
</tr>

<tr>
<td><button type="submit" name="btn-signup" class="button">Sign Up</button></td>
</tr>
</table>
    </form>
<a href="login form.php"><img src="ucp.PNG" alt="ucp" height="60px" class="ucp" ></a>
<style>
body {
  font-size: 120%;
  background: #F8F8FF;
}
.sideimg{
	margin: 0 auto;
	height: 790px;
	width: 66%;
	color: black;
	position: fixed;
	border: #000000;
	top: -9%;
	left: 39.5%;
	background-image:url("studentlogin-15-14.png");
	background-size: 120%;
	background-repeat: no-repeat;
}
.heading{
 position: fixed;
	color: #146F5E;
  font-size: 44px;
  border-bottom: 5px solid #146F5E;
  font-family:Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
	font-weight: bold;
	left: 11.5%;
	top:15%;
}
.ucp 
{
	position: fixed;
	top: 0%;
	left: 11.8%;
	
}
form {
  top: 33%;
	left:11.5%;
	position: fixed;
  background: transparent;
  border: none;
  
}
.form input {
  margin: 10px 0px 15px 0px;
}

.form input {
  border: none;
  outline: none;
  background:#146F5E;
  color: #DFFEFF;
  font-size: 20px;
  height: 36px;
  width: 300px;
  border-radius: 100px;
  margin: 10px;
  text-align: center;
  overflow: hidden;
}
.form input:hover{
	box-shadow: 0px 0px 2px 2px #146F5E;
}

.button {
    width: 10%;
  background: none;
  border: 2px solid #146F5E;
  color:black;
  padding: 3px;
  font-size: 16px;
  cursor: pointer;
  top: 91%;
  left:18%;
  border-radius: 20px;
  position: fixed;
}
.button:hover{
	background-color: #146F5E;
  color:white;
	font-family:Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
}
.heading{
 position: fixed;
	color: #146F5E;
  font-size: 44px;
  border-bottom: 5px solid #146F5E;
  font-family:Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
	font-weight: bold;
	left: 11.5%;
	top:15%;
}
.ucp 
{
	position: fixed;
	top: 0%;
	left: 11.8%;
	
}
.error{
    color: red;
    position: fixed;
    left: 17%;
    top: 35%;
    font-family:Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: large;
}


</style>
	</body>
</html>
