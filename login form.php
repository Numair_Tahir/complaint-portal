<?php include("include/config.php"); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="login.css">
  </head>
  <body style="background-color:#DFFEFF";>
     
    <form action="login-action.php" method="Post">
      
      <div class="formcontainer">
      <div class="container">
      <div class="login-box">
            <h1>LOGIN</h1>
        
        <input type="text" placeholder="User-ID" name="uid" 
         <?php  
            if(isset($_COOKIE["uid"]))
            {
              echo "value='" . $_COOKIE["uid"] . "'";
            }
         ?>
        required><br>
        
        <input type="password" placeholder="Password" name="pw" required>
      </div>
       <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);
       

        if($msg=="SR"){
          echo "<p><strong class='success'>SUCCESS: </strong>Registered Successfully.</p>";
         }
      if($msg=="UAAA"){
          echo "<p><strong class='error'>ERROR :</strong> Unauthorized Access Attempt!<br>You Must Login to Access.</p>";
        }

        if($msg=="PWNM"){
          echo "<p><strong class='error'>ERROR :</strong> Password Did Not Matched !</p>";
        }

        if($msg=="IDNM"){
          echo "<p><strong class='error'>ERROR :</strong> User-Id Not Found !</p>";
        }

        if($msg=="AS"){
          echo "<p><strong class='error'>PROBLEM :</strong> Your account was suspended.Please contact admin for further details</p>";
        }
      }

    ?>
      <button type="submit" name="submit">Login</button>
      <div class="link"><a href="Student Registration.php">Sign Up</a></div>
      </div>
      </div>

      
    </form>
    <a href="Home Page.php"><img src="ucp.PNG" alt="ok" height="60px" class="ucp" ></a> 
  </body>
</html>
