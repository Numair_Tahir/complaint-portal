<?php  
    
     include("include/config.php"); 
   
    if((isset($_POST["uid"])) && (isset($_POST["pw"])))
    {
    	$uid =sanitizeInput($_POST["uid"]);
      $pw = sanitizeInput($_POST["pw"]);
    	
       $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }
       //for query//
       $sql='SELECT * FROM user WHERE user_id = ?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('i',$uid);
       $stmt->execute();
       $stmt->store_result();

       echo "Found " . $stmt->num_rows . " records.<br />";

       if ($stmt->num_rows==1) {
       	# code...

       $stmt->bind_result($userid,$email,$hpwd,$phoneno,$name,$type,$status);
       while ($stmt->fetch()) {
       	if($status=="A"){
          $matched =password_verify($pw, $hpwd);
       	

        if($matched){

          $_SESSION["loggedin"]=true;
          
          setcookie("uid", $userid, time()+ 60*60*24*30);
       
        }
        else{
           header("Location: login form.php?msg=PWNM");
           exit;
        }


        $_SESSION["usid"]=$userid;
        $_SESSION["uemail"]=$email;
        $_SESSION["utype"]=$type;
        $_SESSION["ustatus"]=$status;

     }

     else{
     	header("Location: login form.php?msg=AS");
     	exit;
     }
    }
       $stmt->free_result();
       $stmt->close();
   
     }

     else{
        header("Location: login form.php?msg=IDNM");
        exit; 
     }

      //for database close//
        $conn->close();
    } 

   else{
        header("Location: login form.php");
        exit;
    }

    header("Location: index.php");
    exit;
