<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Departments</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="index.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    <div id="block">
    
      <h1>All Departments</h1>

       
        <h3>Current Departments</h3>
        <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SDC"){
          echo "<p><strong class='success'>SUCCESS: </strong> Deleted successfully.</p>";
        }


      }

    ?>

<?php
     $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='SELECT dep_id, dep_name,dep_campus,dep_contact FROM department';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->execute();
       $stmt->store_result();

       if ($stmt->num_rows > 0) {
        echo "<table>";
          echo "<tr>";
          echo "<th>Department id</th>";
          echo "<th>Department Name</th>";
          echo "<th>Department Campus</th>";
          echo "<th>Department Contact</th>";
          echo "<th>&nbsp</th>";
          echo "</tr>";

       $stmt->bind_result($did,$dname,$dcampus,$dcontact);
       while ($stmt->fetch()) {
          
          
          echo "<tr>";
          echo "<td>$did</td>";
          echo "<td>$dname</td>";
          echo "<td>$dcampus</td>";
          echo "<td><div>$dcontact</div></td>";
          echo "<td><form action='del-department.php' method='post'><input type='hidden' value='$did' name='depid'/><input type='submit' value='Delete'/></form></td>";
          echo "</tr>";
       }
       echo "</table>";
       $stmt->free_result();
       $stmt->close();
   
     }
      else{
        echo "<p>No Records Found</p>";
       }

     
        $conn->close();
?>
      </div>
      
      
      
      
      

  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>