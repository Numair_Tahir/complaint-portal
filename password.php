<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="password.css">
  </head>
  <body>
    <?php include("include/header.php"); ?> 
    <div id="container">
    <form action="password-action.php" method="post">
      <h1>Update Password</h1>
      <div class="formcontainer">
      <div class="container">
        <label for="npsw1"><strong>New Password</strong></label><br>
        <input type="password" name="pw1" required><br>
        <label for="cpsw1"><strong>Confirm Password</strong></label><br>
        <input type="password"  name="pw2" required>
      
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SUPW"){
          echo "<p><strong class='success'>SUCCESS: </strong> Password updated successfully.</p>";
        }

        if($msg=="PWDNM"){
          echo "<p><strong class='error'>ERROR :</strong> Both passwords are not same !</p>";
        }


      }

    ?>
      <button type="submit" name="submit">Change Password</button>

      
    </form>
    </div>
  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>