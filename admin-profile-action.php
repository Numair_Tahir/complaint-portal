<?php  
    
     include("include/config.php"); 

    if((isset($_POST["phn"])) && (isset($_POST["fn"])) && (isset($_POST["eml"])))
    {
      $phn =sanitizeInput($_POST["phn"]);
      $fn = sanitizeInput($_POST["fn"]);
      $eml = sanitizeInput($_POST["eml"]);

      

       
       $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' . $conn->connect_error, E_USER_ERROR);
     
       }
       //for query//
       $sql='UPDATE user SET ph_no=?, name=?, email=? WHERE user_id=?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('issi',$phn,$fn,$eml,$_SESSION["usid"]);
       $stmt->execute();
        $stmt->close();

      //for database close//
        $conn->close();
        header("Location: admin profile.php?msg=SUP");
        exit;

        

   }

   