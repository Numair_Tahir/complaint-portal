<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="U")){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Complaint Status</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="password.css">
  </head>
  <body>
    <?php include("include/header.php"); ?> 
    <div id="block">
    
      <h1>All of your Complaint Status</h1>
        <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SDC"){
          echo "<p><strong class='success'>SUCCESS: </strong> Deleted successfully.</p>";
        }


      }

    ?>

<?php
     $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);
       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql="SELECT comp_id, profession, status, user_id FROM complaint WHERE user_id=?";
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

      $stmt->bind_param('i',$_SESSION["usid"]);
       $stmt->execute();
       $stmt->store_result();
        $stmt->bind_result($comid,$profession,$status,$userid);

       if ($stmt->num_rows > 0) {
        echo "<table>";
          echo "<tr>";
          echo "<th>Complain id</th>";
          echo "<th>Profession</th>";
          echo "<th>Complaint</th>";
          echo "<th>Status</th>";
          echo "<th>User id</th>";
          echo "</tr>";
       while ($stmt->fetch()) {
          $_SESSION["compid"]=$comid;
          $_SESSION["usid"]=$userid;

          
          echo "<tr>";
          echo "<td>$comid</td>";
          echo "<td>$profession</td>";
          echo "<td><form action='update-complain-user.php' method='post'><input type='hidden' value='$userid' name='usid'/><input type='hidden' value='$comid' name='compid'/><input type='submit' value='Complain'/></form></td>";
          echo "<td>$status</td>";
          echo "<td>$userid</td>";
          echo "</tr>";

       }
       echo "</table>";

       $stmt->free_result();
       $stmt->close();
   
     }
      else{
        echo "<p>No Records Found</p>";
       }

     
        $conn->close();
?>
      </div>
      
      
      
      
      

  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>