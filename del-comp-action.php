<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){

  if (isset($_POST["compid"])){
    $comid=sanitizeInput($_POST["compid"]);

  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='DELETE FROM complaint WHERE comp_id = ?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('i',$comid);
       $stmt->execute();
       $stmt->close();
 
        # code...
        //for database close//
        $conn->close();
        header("Location: manage complaints.php?msg=SDC");
        exit;
  }

}
else{
 header("Location: login.php?msg=UAAA");
        

}


 