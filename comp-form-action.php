<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="U")){

  if ((isset($_POST["ctitle"])) && (isset($_POST["prof"])) && (isset($_POST["description"])) && (isset($_POST["dep"]))){
    $ctitle=sanitizeInput($_POST["ctitle"]);
    $prof=sanitizeInput($_POST["prof"]);
    $description=sanitizeInput($_POST["description"]);
    $depname=sanitizeInput($_POST["dep"]);
    
    
    
  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }
        $sql="INSERT INTO complaint (comp_title, profession, comp_description, user_id , dep_id) VALUES (?,?,?,?,?)";
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }
       

       $stmt->bind_param('sssii',$ctitle,$prof,$description,$_SESSION["usid"],$depname) ;
       $stmt->execute();
       $stmt->close();
 
        # code...
        //for database close//
        $conn->close();
        header("Location: add complaint.php?msg=SAC");
        exit;
        
  }
}