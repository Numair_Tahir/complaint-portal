<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){

  if (isset($_POST["uid"])){
    $uid=sanitizeInput($_POST["uid"]);

  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='SELECT * FROM complaint WHERE comp_id = ?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('i',$uid);
       $stmt->execute();
       $stmt->store_result();

       $users=$stmt->num_rows;
        # code...
        //for database close//
        $conn->close();
        
}

 

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="index.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    <div id="block">

      <div class="mng">
        <h3>Delete Users</h3>

 <?php
    if($users > 0){
      echo "<p><strong class='error'>Sorry :</strong>You cannot Delete this record !</p>";
    }

    else{
      echo "<p><strong class='error'>Warning :</strong> Are You Sure you want to Delete this record? !</p>";
      echo "<form action='del-user-action.php' method='post'>";

      echo "<input type='hidden' name='uid' value='$uid'/>";
      echo "<input type='submit' value='Delete'/>";
      echo "</form>";
      
    }
 ?>
      </div>
      
      
      
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SDC"){
          echo "<p><strong class='success'>SUCCESS: </strong> Deleted Successfully.</p>";
         }
      }

    ?>
      

  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>