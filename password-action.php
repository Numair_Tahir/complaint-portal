<?php  
    
     include("include/config.php"); 

    if((isset($_POST["pw1"])) && (isset($_POST["pw2"])))
    {
      $pw1 =sanitizeInput($_POST["pw1"]);
      $pw2 = sanitizeInput($_POST["pw2"]);

      if ($pw1==$pw2) {

        $hpwd=password_hash($pw1, PASSWORD_DEFAULT);
       
       $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' . $conn->connect_error, E_USER_ERROR);
     
       }
       //for query//
       $sql='UPDATE user SET password=? WHERE user_id=?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('si',$hpwd,$_SESSION["usid"]);
       $stmt->execute();
        $stmt->close();

      //for database close//
        $conn->close();
        header("Location: password.php?msg=SUPW");
        exit;
    } 

   else{
        header("Location: password.php?msg=PWDNM");
        exit;
    }

   }

   