<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){

  if (isset($_POST["uid"])){
    $uid=sanitizeInput($_POST["uid"]);

  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='DELETE FROM user WHERE user_id = ?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('i',$uid);
       $stmt->execute();
       $stmt->close();
 
        # code...
        //for database close//
        $conn->close();
        header("Location: view-users.php?msg=SDC");
        exit;
       
  }

}
else{
 header("Location: login.php?msg=UAAA");
        

}