<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){ 
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST["compid"])){
    $comid=sanitizeInput($_POST["compid"]);
$sql = "SELECT comp_title, comp_description FROM complaint WHERE comp_id=?";
$stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }
       $stmt->bind_param('i',$comid);
       $stmt->execute();
       $stmt->store_result();
       
       $stmt->bind_result($comptitle,$compdesc);
       $stmt->fetch();
      
       $stmt->free_result();
       $stmt->close();

      //for database close//
        $conn->close();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    <div id="complain">

      <div class="mng">
        <h1>Complain Title :</h1>
          <?php 
            echo "<p class='complain'>$comptitle</p>";
          ?>
      
    <h1>Complain Description :</h1>
              <?php
            echo "<p class='complain'>$compdesc</p>";
          ?>
      

  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>