<?php  
    
     include("include/config.php"); 

    if((isset($_POST["uid"])) && (isset($_POST["status"])))
    {
      $uid =sanitizeInput($_POST["uid"]);
      $status = sanitizeInput($_POST["status"]);

      if ($status=="A") {
        $newstatus="S";
      }
      else{
        $newstatus="A";
      }

        $hpwd=password_hash($pw1, PASSWORD_DEFAULT);
       
       $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' . $conn->connect_error, E_USER_ERROR);
     
       }
       //for query//
       $sql='UPDATE user SET status=? WHERE user_id=?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('si',$newstatus,$uid);
       $stmt->execute();
        $stmt->close();

      //for database close//
        $conn->close();
        header("Location: view-users.php?msg=SUS");
        exit;
    } 

    

   

   