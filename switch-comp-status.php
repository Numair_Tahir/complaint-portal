<?php  
    
     include("include/config.php"); 

    if((isset($_POST["compid"])) && (isset($_POST["status"])))
    {
      $compid =sanitizeInput($_POST["compid"]);
      $status = sanitizeInput($_POST["status"]);

      if ($status=="pending") {
        $newstatus="Approved";
      }
      else{
        $newstatus="pending";
      }
       
       $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' . $conn->connect_error, E_USER_ERROR);
     
       }
       //for query//
       $sql='UPDATE complaint SET status=? WHERE comp_id=?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('si',$newstatus,$compid);
       $stmt->execute();
        $stmt->close();

      //for database close//
        $conn->close();
        header("Location: manage complaints.php?msg=SUS");
        exit;
    } 

    

   

   