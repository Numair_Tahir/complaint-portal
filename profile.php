<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='SELECT name, ph_no, email FROM user WHERE user_id = ?';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('i',$_SESSION["usid"]);
       $stmt->execute();
       $stmt->store_result();

       if ($stmt->num_rows==1) {
        # code...

       $stmt->bind_result($phoneno,$name,$email);
       $stmt->fetch();
       $stmt->free_result();
       $stmt->close();
   
     }


      //for database close//
        $conn->close();

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="password.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
   <div id="container">
    <form action="profile-action.php" method="post">
      <h1>Update User Details</h1>
      <div class="formcontainer">
      <div class="container">
        <label for="lphno"><strong>Phone No</strong></label><br>
        <input type="text" class="txt" name="phn" value="<?php echo $name; ?>" required><br>
        
        <label for="lname"><strong>Name</strong></label><br>
        <input type="text" class="txt" name="fn" value="<?php echo $phoneno; ?>" required><br>

        <label for="lemale"><strong>Email</strong></label><br>
        <input type="text" class="txt" name="eml" value="<?php echo $email; ?>" required><br>
      
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SUP"){
          echo "<p><strong class='success'>SUCCESS: </strong> Details updated successfully.</p>";
        }
     
      }

    ?>
     
    <button type="submit" name="submit">Update</button>
   
      
      
    </div>
    </form>
   </div>
  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>