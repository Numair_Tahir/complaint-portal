<?php include("include/config.php");?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Registeration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="signup form.css">
  
  </head>
  <body onload="document.registration.fname.focus();">
    <div class="main-block">
      <div class="left-part">
        <h1>University Complain Portal</h1>
        <p><strong>Welcome to University Complain Portal</strong></p>
        <strong>Please Register Your account for using this portal</strong>
        <p>From this portal you can submit your complaints about University management,teachers,students or any other issues which happen in previous seminars and events. You can also check your upcoming events and seminar by using this portal</p>
      </div>
      <form name="registration" onsubmit="return signupvalidation();" action="signup-action.php" method="post">
        <div class="title"> 
          <h2>Register here</h2>
        </div>
        <div class="info">
          <input class="fname" type="text" name="fname" placeholder="Full name" required/>
          <input type="text" name="id" placeholder="Sapid/Employee id" required/>
          <input type="text" name="email" placeholder="Email" required/>
          <input type="text" name="phno" placeholder="Phone No" required/>
          <input type="password" name="pw" placeholder="Password" required/>
          <input type="password" name="cpw" placeholder="Confirm Password" required/>
        </div>
        
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SR"){
          echo "<p><strong class='success'>SUCCESS: </strong>Registered Successfully.</p>";
         }
      }

    ?>
        <button type="submit" name="submit">Signup</button><br><br>
        <p>If you already have account<a class="link" href="login form.php">Click here!</a></p>
         
      </form>
    </div>
  </body>
</html>
<script src="js/signup.js"></script>