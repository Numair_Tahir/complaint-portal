<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Departments</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="index.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    
    
      <h1 class="hd">All Users</h1>
      <form action="view-users.php" method="get">
       <input class="srch" type="text" name="us" required placeholder="Search By Name" style="width: 20%" />
        <input class="btnsrch" type="submit" value="Search"/>
        
      </form>
      <a href="db.php"><input class="btnsrch2" type="submit" value="Export"/></a>
      <div id="block">
   <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SDC"){
          echo "<p><strong class='success'>SUCCESS: </strong> Deleted Successfully.</p>";
        }

        if($msg=="SUS"){
          echo "<p><strong class='success'>SUCCESS: </strong> Status Updated Successfully.</p>";
        }


      }

    ?>
<div class="table">
<?php
     $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }
         
       if((isset($_GET["us"])))
       {
        $us=sanitizeInput($_GET["us"]);
        $sql="SELECT user_id, email, ph_no, name,type, status FROM user WHERE (user_id LIKE '%$us%' OR email LIKE '%$us%' OR name LIKE '%$us%')AND type='U' ORDER BY user_id";

       }


       else{
        $sql="SELECT user_id, email, ph_no, name,type, status FROM user WHERE type='U' ORDER BY email";
       }

        
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->execute();
       $stmt->store_result();

       if ($stmt->num_rows > 0) {
        echo "<table>";
          echo "<tr>";
          echo "<th>User id</th>";
          echo "<th>Email</th>";
          echo "<th>Name</th>";
          echo "<th>Phone no</th>";
          echo "<th>Type</th>";
          echo "<th>Status</th>";
          echo "<th>&nbsp</th>";
          echo "<th>&nbsp</th>";
          echo "</tr>";

       $stmt->bind_result($uid,$uem,$uphno,$uname,$utype,$ustatus);
       while ($stmt->fetch()) {
          
          
          echo "<tr>";
          echo "<td>$uid</td>";
          echo "<td>$uem</td>";
          echo "<td>$uname</td>";
          echo "<td>$uphno</td>";
          echo "<td>$utype</td>";
          echo "<td><div>$ustatus</div></td>";
          echo "<td><form action='switch-user.php' method='post'><input type='hidden' value='$uid' name='uid'/><input type='hidden' value='$ustatus' name='status'/><input type='submit' value='Switch'/></form></td>";
          echo "<td><form action='del-user.php' method='post'><input type='hidden' value='$uid' name='uid'/><input type='submit' value='Delete'/></form></td>";
          echo "</tr>";
       }
       echo "</table>";
       $stmt->free_result();
       $stmt->close();
   
     }
      else{
        echo "<p>No Records Found</p>";
       }

     
        $conn->close();
?>
</div>
      </div>
      
      
      
      
      

  </div>
  
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>