<?php include("include/config.php"); 
 
  if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
     
 ?>


<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' type='text/css' href='complaint form1.css'>
<title><?php  echo $SITE_TITLE; ?></title>
</head>
<body>
<?php include("include/header.php"); ?>

<div class="container">
  <h1 class="heading">Add Your Complaint</h1>
  <form action="comp-form-action.php" method="post">

    <label for="ctitle1">Complaint Title</label><br>
    <input type="text" name="ctitle" required><br>

     
    <br>
    <label for="profession1">Profession</label><br>
    <input type="radio" id="rb1" name="prof" value="Student" checked><label for="rb1">Student</label>
    <input type="radio" id="rb2" name="prof" value="Teacher"><label for="rb2">Teacher/Professor</label>
    <input type="radio" id="rb3" name="prof" value="Employee"><label for="rb3">Employee</label>
    <br>
    
    <label for="dep">Department</label><br>
    <select name="dep" required>
      <option value="">Select</option>
   <?php  
     $conn = new mysqli($DB_SERVER, $DB_USER, $DB_PASSWORD, $DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql='SELECT dep_id,dep_name FROM department ORDER BY dep_name';
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->execute();
       $stmt->store_result();

       if ($stmt->num_rows > 0) {
        

       $stmt->bind_result($depid,$depname);
       while ($stmt->fetch()){
          echo "<option value='$depid'>$depname</option>";
          
    
       }
       
       $stmt->free_result();
       $stmt->close();
   
     }
      else{
        echo "<option value='0'>Uncategorized</option>";
       }

     
        $conn->close();

   ?>
    </select><br>

  
    
    <label for="subject">Description</label><br>
    <textarea name="description" class="complain" placeholder="Write something.." style="height:200px"></textarea><br>
   
       
    If you Upload document complaint file then&nbsp<a href="file.php" class="button">Click  here!</a> Size Limit = 5mb<br><br>
    <br>
<?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SAC"){
          echo "<p><strong class='success'>SUCCESS: </strong>Your Complaint was Submitted Successfully.</p>";
         }

         if($msg=="NAC"){
          echo "<p><strong class='error'>ERROR: </strong> Complaint was not added.Plz fill it again !</p>";
         }
      }

    ?>
    <input type="submit" name="submit" value="Submit"></button>
  </form>
</div>

</body>
</html>


<?php  
  }
  
  else{
    header("Location: login form.php?msg=UAAA");
  }
?>