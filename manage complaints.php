<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Manage Complains</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="index.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    <div id="block">
    
      <h1 class="hd">All Complaints</h1>

      <div class="mng">
        <form action="manage complaints.php" method="get">
        <input type="text" name="us" required placeholder="Search By ID" style="width: 20%" />
        <input type="submit" value="Search" class="search"/>
      </form>

<?php
     $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }
       if((isset($_GET["us"])))
       {
        $us=sanitizeInput($_GET["us"]);
        $sql="SELECT comp_id, profession, status, user_id, dep_id FROM complaint WHERE (comp_id LIKE '%$us%' OR profession LIKE '%$us%' OR user_id LIKE '%$us%' OR dep_id LIKE '%$us%') ORDER BY comp_id";

       }


       else{
             $sql='SELECT comp_id, profession, status, user_id, dep_id FROM complaint';
       }


       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->execute();
       $stmt->store_result();

       if ($stmt->num_rows > 0) {
        echo "<table>";
          echo "<tr>";
          echo "<th>Complain id</th>";
          echo "<th>Profession</th>";
          echo "<th>Complaint</th>";
          echo "<th>Status</th>";
          echo "<th>User id</th>";
          echo "<th>Department id</th>";
          echo "<th>&nbsp</th>";
          echo "<th>&nbsp</th>";

          echo "</tr>";

       $stmt->bind_result($comid,$profession,$status,$userid,$depid);
       while ($stmt->fetch()) {
          
          
          echo "<tr>";
          echo "<td>$comid</td>";
          echo "<td>$profession</td>";
          echo "<td><form action='complain.php' method='post'><input type='hidden' value='$comid' name='compid'/><input type='submit' value='Complain'/></form></td>";
          echo "<td>$status</td>";
          echo "<td>$userid</td>";
          echo "<td>$depid</td>";
          echo "<td><form action='switch-comp-status.php' method='post'><input type='hidden' value='$comid' name='compid'/><input type='hidden' value='$userid' name='usid'/><input type='hidden' value='$status' name='status'/><input type='submit' value='Switch'/></form></td>";
           echo "<td><form action='del-comp.php' method='post'><input type='hidden' value='$comid' name='compid'/><input type='submit' value='Delete'/></form></td>";
          echo "</tr>";
       }
       echo "</table>";
       $stmt->free_result();
       $stmt->close();
   
     }
      else{
        echo "<p>No Records Found</p>";
       }

     
        $conn->close();
?>
      </div>
      
      
      
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SDC"){
          echo "<p><strong class='success'>SUCCESS: </strong> Deleted successfully.</p>";
        }

        if($msg=="SUS"){
          echo "<p><strong class='success'>SUCCESS: </strong> Status updated successfully.</p>";
        }



      }

    ?>
      

  </div>
  </body>
</html>
<?php  
  }

  else{
    header("Location: login form.php?msg=UAAA");
  }
?>