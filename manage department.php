<?php include("include/config.php"); 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] && ($_SESSION["utype"]=="A")){
  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title>Department Details</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="password.css">
  </head>
  <body>
    <?php include("include/admin header.php"); ?> 
    <div id="container">
    <form action="manage dep-action.php" method="post">
      <h1>Add Department Details</h1>
      <div class="formcontainer">
      <div class="container">
        
        <input type="text" placeholder="Deparment Name"  name="dname" required><br>
        
        
        <input type="text" placeholder="Campus" name="dcampus" required><br>

        
        <input type="text" placeholder="Department Contact" name="dcontact" required><br>
      
      <?php
      if(isset($_GET["msg"])){
        $msg=sanitizeInput($_GET["msg"]);

        if($msg=="SA"){
          echo "<p><strong class='success'>SUCCESS: </strong> Details added successfully.</p>";
        }

         if($msg=="DNA"){
          echo "<p><strong class='error'>Sorry: </strong> Details are not added.</p>";
        }
     
      }

    ?>
    </div>
      <button type="submit" name="submit">Add</button>
      
    </div>
    </form>
  </div>
  </body>
</html>
<?php  
  }
  else{
    header("Location: login form.php?msg=UAAA");
  }
?>