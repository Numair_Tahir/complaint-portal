<?php include("include/config.php"); 


  if ((isset($_POST["fname"])) && (isset($_POST["id"])) && (isset($_POST["address"]))){
    $fname=sanitizeInput($_POST["fname"]);
    $id=sanitizeInput($_POST["id"]);
    $address=sanitizeInput($_POST["address"]);
 
    
  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql="INSERT INTO student (reg_no, name, address) VALUES (?,?,?)";
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('sss',$id,$fname,$address);
       $stmt->execute();
       $stmt->close();
 
        # code...
        //for database close//
        $conn->close();
        header("Location: login form.php?msg=SR");
        exit;
  }




?>