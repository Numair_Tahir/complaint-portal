<?php 
session_start();
$db = mysqli_connect('localhost', 'root', '', 'universty complaint portal');

if(isset($_POST['btn-signup']))
{
 $username = trim($_POST['uid']);
 $email = trim($_POST['email']);
 $name = trim($_POST['name']);
 $ph_no = trim($_POST['ph_no']);
 $pass = trim($_POST['password']);
 $cpass = trim($_POST['password_2']);
 $errors = array(); 

 

 if(empty($username))
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Enter User-ID!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter Your User-ID!";
  $code = 1;
 }
 else if(!ctype_digit($username))
 {
   function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Enter Digits Only In User-ID!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter Digits Only In User-ID!";
  $code = 1;
 }
 else if(strlen($username) < 4 || strlen($username) > 4 )
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Only 4 Digits Allowed In User-ID!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Please Enter 4 Digits!";
  $code = 4;
 }
 else if(empty($email))
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Please Enter Email!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter Your Email!";
  $code = 2;
 }
 else if(!preg_match("/^[_.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+.)+[a-zA-Z]{2,6}$/i", $email))
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Enter Valid Email!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter Valid Email!";
  $code = 2;
 }

 else if(empty($pass))
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Please Enter Password!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter Password!";
  $code = 4;
 }
 else if(strlen($pass) < 4 )
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Please Enter 4 Characters!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Enter 4 Characters!";
  $code = 4;
 }
 else if($pass!=$cpass )
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Password Didn't Matched!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Password Not Match!";
  $code = 5;
 }
 else if(empty($ph_no))
 {
  function function_alert($message) 
  {
    echo "<script>alert('$message');</script>";
  }
    
  function_alert("Enter Phone Number!");
  
  echo "<script>location.href='Student Registration.php'</script>";
  $error = "Password Not Match!";
  $code = 5;
 }
 
 else
 {
      $user_check_query = "SELECT * FROM user WHERE user_id='$uid' OR email='$email' LIMIT 1";
      $result = mysqli_query($db, $user_check_query);
      $user = mysqli_fetch_assoc($result);
      
     
    
      // Finally, register user if there are no errors in the form
      if (count($errors) == 0) {
            $password = ($password_1);//encrypt the password before saving in the database
    
            $query = "INSERT INTO user (user_id, email, password,ph_no,name) 
                          VALUES('$username', '$email', '$cpass','$ph_no','$name')";
            mysqli_query($db, $query);
            $_SESSION['uid'] = $username;
            $_SESSION['success'] = "You are now logged in";
            header('location: login form.php');
      }
 }
}
?>