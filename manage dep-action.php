<?php include("include/config.php");


          


  if ((isset($_POST["dname"])) && (isset($_POST["dcampus"])) && (isset($_POST["dcontact"]))){
    $dname=sanitizeInput($_POST["dname"]);
    $dcampus=sanitizeInput($_POST["dcampus"]);
    $dcontact=sanitizeInput($_POST["dcontact"]);
    
  
  
  $conn = new mysqli($DB_SERVER,$DB_USER,$DB_PASSWORD,$DB_NAME);

       if ($conn->connect_error) {
              trigger_error('Database connection failed: ' .$conn->connect_error, E_USER_ERROR);
     
       }

        $sql="INSERT INTO department (dep_name, dep_campus, dep_contact) VALUES (?,?,?)";
       $stmt = $conn->prepare($sql);
       if($stmt === false) {
            
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
       }

       $stmt->bind_param('ssi',$dname,$dcampus,$dcontact);
       $stmt->execute();
       $stmt->close();
 
        # code...
        //for database close//
        $conn->close();
        header("Location: manage department.php?msg=SA");
        
  }
 


  